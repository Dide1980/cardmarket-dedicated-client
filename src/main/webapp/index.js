
var Logger = {
		result : $('.result'),
		info : function(msg){
			Logger.result.html(Logger.result.html()+'<p>'+msg+'</p>');
		},
		warn : function(msg){
			Logger.result.html(Logger.result.html()+'<p style="color:red">'+msg+'</p>');
		}
}

$('.say-hello').click(() => {
	$.get('/camel/say/hello', undefined, function(data, textStatus, jqXHR) {
		Logger.info(data.msg);
	}).fail(function(event, jqXHR, thrownError) {
		Logger.warn( thrownError.message);
	});
});
