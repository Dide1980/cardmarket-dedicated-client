package ch.blanquet.mkm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import ch.blanquet.mkm.beans.Product;

public class ProductsBase64Deserializer extends JsonDeserializer<Collection<Product>> {

	@Override
	public Collection<Product> deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {

		final String value = p.getValueAsString();
		String csvContent = Base64Zipped.decode(value);

		final BufferedReader r = new BufferedReader(new StringReader(csvContent));

		final List<Product> result = new ArrayList<Product>();

		String csvLine;
		boolean header = true;
		while ((csvLine = r.readLine()) != null) {
			if (header) {
				header = false;
				continue;
			}

			try {
				result.add(Product.newInstance(parseCsvLine(csvLine)));
			} catch (ParseException e) {
				throw new IOException(e);
			}
		}

		return result;
	}

	private static List<String> parseCsvLine(final String line) {
		final List<String> result = new ArrayList<String>();

		boolean inValue = false;
		String currentValue = "";

		for (int i = 0; i < line.length(); i++) {
			final char c = line.charAt(i);
			if (currentValue.isEmpty() && !inValue && c == '"') {
				inValue = true;
			} else if (inValue && c == '"') {
				inValue = false;
			} else if (!inValue && c == ',') {
				result.add(currentValue);
				currentValue = "";
			} else if (!currentValue.isEmpty() && !inValue && c == '"') { // Double quote
				currentValue += c;
				inValue = true;
			} else {
				currentValue += c;
			}
		}

		result.add(currentValue);

		return result;
	}
}
