package ch.blanquet.mkm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Start {
	public static void main(final String[] args) throws Exception {
		SpringApplication.run(Start.class, args);
	}
}
