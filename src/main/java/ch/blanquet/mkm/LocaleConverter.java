package ch.blanquet.mkm;

import java.util.Locale;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class LocaleConverter implements AttributeConverter<Locale, String> {

	public String convertToDatabaseColumn(Locale locale) {
		if (locale != null) {
			return locale.toLanguageTag();
		}
		return null;
	}

	public Locale convertToEntityAttribute(String languageTag) {
		if (languageTag != null && !languageTag.isEmpty()) {
			return Locale.forLanguageTag(languageTag);
		}
		return null;
	}

}
