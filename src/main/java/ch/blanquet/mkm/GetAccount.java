package ch.blanquet.mkm;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import ch.blanquet.mkm.beans.Response;

@Component
public class GetAccount extends RouteBuilder {

	private final static String url = "https://api.cardmarket.com/ws/v1.1/account";

	@Override
	public void configure() throws Exception {
		from("direct:getAccount").setBody(simple(url)).bean(MkmClient.class, "request");

		from("direct:parseAccount").setBody(simple(url)).bean(MkmClient.class, "request").unmarshal()
				.jacksonxml(Response.class).marshal().json();

		from("direct:xmlToJson").setBody(simple(url)).bean(MkmClient.class, "request").unmarshal().jacksonxml()
				.marshal().json();
	}

}
