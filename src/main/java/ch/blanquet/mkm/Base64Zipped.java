package ch.blanquet.mkm;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import org.apache.commons.compress.utils.IOUtils;

public class Base64Zipped {
	public static String decode(final String content) throws IOException {
		final byte[] zip = java.util.Base64.getDecoder().decode(content);

		InputStream gzipIs = new GZIPInputStream(new ByteArrayInputStream(zip));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		IOUtils.copy(gzipIs, baos);
		gzipIs.close();

		return new String(baos.toByteArray());
	}
}
