package ch.blanquet.mkm;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class BooleanDeserializer extends JsonDeserializer<Boolean> {

	private final static String[] TRUE_VALUES = new String[] { "1", "yes", "true", "oui" };

	private final static String[] FALSE_VALUES = new String[] { "0", "no", "false", "non" };

	@Override
	public Boolean deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		try {
			return p.getValueAsBoolean();
		} catch (Exception e) {
			final String value = p.getValueAsString();
			for (final String v : TRUE_VALUES) {
				if (v.equalsIgnoreCase(value))
					return true;
			}

			for (final String v : FALSE_VALUES) {
				if (v.equalsIgnoreCase(value))
					return false;
			}

			throw new IOException("Impossible de parser la valeur " + value + " comme un boolean");
		}
	}
}
