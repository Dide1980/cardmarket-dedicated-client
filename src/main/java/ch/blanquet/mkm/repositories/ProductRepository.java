package ch.blanquet.mkm.repositories;

import org.springframework.data.repository.CrudRepository;

import ch.blanquet.mkm.beans.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
