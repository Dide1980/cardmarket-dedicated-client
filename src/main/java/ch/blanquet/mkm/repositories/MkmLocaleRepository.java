package ch.blanquet.mkm.repositories;

import java.util.Locale;

import org.springframework.data.repository.CrudRepository;

import ch.blanquet.mkm.beans.MkmLocale;

public interface MkmLocaleRepository extends CrudRepository<MkmLocale, Long> {
	public MkmLocale findByLocale(final Locale locale);
}
