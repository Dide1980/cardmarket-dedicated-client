package ch.blanquet.mkm.repositories;

import org.springframework.data.repository.CrudRepository;

import ch.blanquet.mkm.beans.Expansion;

public interface ExpansionRepository extends CrudRepository<Expansion, Long> {
	public Expansion findByExpansionName(final String name);
	public Expansion findByExpansionID(final Long id);
}
