package ch.blanquet.mkm.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ch.blanquet.mkm.beans.Localization;

public interface LocalizationRepository extends CrudRepository<Localization, Long> {
	
	@Query(value = "SELECT a FROM Localization a WHERE a.locale.idLanguage = :id_language AND a.name = :name")
	public Localization findLocalization(@Param("id_language") final long idLanguage, @Param("name") final String name);
	
}
