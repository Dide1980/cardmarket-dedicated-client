package ch.blanquet.mkm.repositories;

import org.springframework.data.repository.CrudRepository;

import ch.blanquet.mkm.beans.Metaproduct;

public interface MetaproductRepository extends CrudRepository<Metaproduct, Long> {

}
