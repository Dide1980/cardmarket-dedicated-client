package ch.blanquet.mkm.beans;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@Entity
public class Metaproduct extends Element implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9079816066851041991L;
	@Id
	@JacksonXmlProperty(isAttribute = false)
	private long idMetaproduct;
	@JacksonXmlProperty(isAttribute = false, localName = "localization")
	@JacksonXmlElementWrapper(useWrapping = false)
	@ManyToMany(fetch=FetchType.EAGER)
	private List<Localization> localizations;

	private String image;

	public long getIdMetaproduct() {
		return idMetaproduct;
	}

	public void setIdMetaproduct(int idMetaproduct) {
		this.idMetaproduct = idMetaproduct;
	}

	public Collection<Localization> getLocalizations() {
		return localizations;
	}

	public void setLocalizations(List<Localization> localizations) {
		this.localizations = localizations;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
