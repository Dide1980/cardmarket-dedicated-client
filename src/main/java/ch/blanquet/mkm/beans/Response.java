package ch.blanquet.mkm.beans;

import java.io.Serializable;
import java.util.Collection;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import ch.blanquet.mkm.ProductsBase64Deserializer;

public class Response extends Element implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3870227154778947025L;
	private Account account;
	@JacksonXmlProperty(isAttribute = false, localName = "metaproduct")
	private Metaproduct metaproduct;
	@JsonDeserialize(using = ProductsBase64Deserializer.class)
	@JacksonXmlProperty(isAttribute = false, localName = "productsfile")
	private Collection<Product> productsfile;
	@JacksonXmlProperty(isAttribute = false, localName = "product")
	@JacksonXmlElementWrapper(useWrapping = false)
	private Collection<Product> products;
	private String mime;
	private Links links;

	public Metaproduct getMetaproduct() {
		return metaproduct;
	}

	public void setMetaproduct(Metaproduct metaproduct) {
		this.metaproduct = metaproduct;
	}

	public Collection<Product> getProducts() {
		return products;
	}

	public void setProducts(Collection<Product> products) {
		this.products = products;
	}

	public Metaproduct getMetaProduct() {
		return metaproduct;
	}

	public void setMetaProduct(Metaproduct metaProduct) {
		this.metaproduct = metaProduct;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Collection<Product> getProductsfile() {
		return productsfile;
	}

	public void setProductsfile(Collection<Product> productsfile) {
		this.productsfile = productsfile;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public Links getLinks() {
		return links;
	}

	public void setLinks(Links links) {
		this.links = links;
	}
}
