package ch.blanquet.mkm.beans;

import java.util.Locale;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import ch.blanquet.mkm.LocaleConverter;
import ch.blanquet.mkm.LocaleDeserializer;

@Entity
public class MkmLocale {
	@Id
	private long idLanguage;

	@JsonDeserialize(using = LocaleDeserializer.class)
	@JacksonXmlProperty(isAttribute = false, localName = "languageName")
	@Convert(converter = LocaleConverter.class)
	private Locale locale;

	public long getIdLanguage() {
		return idLanguage;
	}

	public void setIdLanguage(long idLanguage) {
		this.idLanguage = idLanguage;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof MkmLocale) {
			MkmLocale l = (MkmLocale) o;
			return l.locale.equals(locale);
		}
		return false;
	}
}
