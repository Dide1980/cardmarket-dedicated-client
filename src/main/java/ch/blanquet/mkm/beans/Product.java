package ch.blanquet.mkm.beans;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@Entity
public class Product extends Element implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2756574660204219789L;

	@Id
	private long idProduct;
	private Long categoryID;
	private String category;
	private String categoryName;

	private Long metacardID = null;
	private Date dateAdded;
	private String website;
	private String image;
	private String gameName;
	private Long idGame;
	private String number;
	private String rarity;

	private Long countArticles;
	private Long countReprints;
	private Long countFoils;
	private Links links;

	@JsonUnwrapped
	@ManyToOne(cascade = CascadeType.ALL)
	private Expansion expansion;

	@JacksonXmlProperty(isAttribute = false, localName = "localization")
	@JacksonXmlElementWrapper(useWrapping = false)
	@ManyToMany(fetch = FetchType.EAGER)
	private Collection<Localization> localizations;

	public Expansion getExpansion() {
		return expansion;
	}

	public void setExpansion(Expansion expansion) {
		this.expansion = expansion;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Long getCountReprints() {
		return countReprints;
	}

	public void setCountReprints(Long countReprints) {
		this.countReprints = countReprints;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String webSite) {
		this.website = webSite;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public Long getIdGame() {
		return idGame;
	}

	public void setIdGame(Long idGame) {
		this.idGame = idGame;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getRarity() {
		return rarity;
	}

	public void setRarity(String rarity) {
		this.rarity = rarity;
	}

	public Long getCountArticles() {
		return countArticles;
	}

	public void setCountArticles(Long countArticles) {
		this.countArticles = countArticles;
	}

	public Long getCountFoils() {
		return countFoils;
	}

	public void setCountFoils(Long countFoils) {
		this.countFoils = countFoils;
	}

	public Links getLinks() {
		return links;
	}

	public void setLinks(Links links) {
		this.links = links;
	}

	public void setMetacardID(Long metacardID) {
		this.metacardID = metacardID;
	}

	public Collection<Localization> getLocalizations() {
		return localizations;
	}

	public void setLocalizations(Collection<Localization> localizations) {
		this.localizations = localizations;
	}

	public long getMetacardID() {
		return metacardID;
	}

	public void setMetacardID(long metacardID) {
		this.metacardID = metacardID;
	}

	public long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(long idProduct) {
		this.idProduct = idProduct;
	}

	public Long getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(Long categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public static Product newInstance(final List<String> csvLine) throws ParseException {
		try {
			final Product result = new Product();

			result.setIdProduct(Long.valueOf(csvLine.get(0)));
			final List<Localization> name = new ArrayList<Localization>();

			final Localization l = new Localization();
			final MkmLocale mkmLocale = new MkmLocale();
			mkmLocale.setIdLanguage(1);
			mkmLocale.setLocale(Locale.ENGLISH);
			l.setLocale(mkmLocale);
			l.setName(csvLine.get(1));
			name.add(l);
			result.setLocalizations(name);

			result.setCategoryID(Long.valueOf(csvLine.get(2)));
			result.setCategory(csvLine.get(3));

			if (!csvLine.get(4).isEmpty()) {
				final Expansion expansion = new Expansion();

				expansion.setExpansionID(Long.valueOf(csvLine.get(4)));
				result.setExpansion(expansion);
			}
			if (!csvLine.get(5).isEmpty())
				result.setMetacardID(Long.valueOf(csvLine.get(5)));
			if (!csvLine.get(6).isEmpty()) {
				final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				result.setDateAdded(sdf.parse(csvLine.get(6)));
			}

			return result;
		} catch (NumberFormatException e) {
			logger.warn("Error while parsing the line " + csvLine.toString());
			throw e;
		} catch (ParseException e) {
			logger.warn("Error while parsing the line " + csvLine.toString());
			throw e;
		}
	}
}
