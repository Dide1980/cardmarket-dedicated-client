package ch.blanquet.mkm.beans;

import java.io.Serializable;

public class Address extends Element implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1416983882620240210L;
	private String name;
	private String street;
	private Integer zip;
	private String city;
	private String country;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Integer getZip() {
		return zip;
	}

	public void setZip(Integer zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
