package ch.blanquet.mkm.beans;

import java.io.Serializable;

public class Links extends Element implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3195544678065516369L;
	
	private String rel;
	private String href;
	private String method;
	public String getRel() {
		return rel;
	}
	public void setRel(String rel) {
		this.rel = rel;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
}
