package ch.blanquet.mkm.beans;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.blanquet.mkm.BooleanDeserializer;

public class Account extends Element implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3720795564520730408L;
	private Integer idUser;
	private String username;
	private String country;
	@JsonDeserialize(using = BooleanDeserializer.class)
	private Boolean isCommercial;
	private Integer riskGroup;
	private Integer reputation;
	private Integer shipsFast;
	private Integer sellCount;
	private Boolean onVacation;
	private Integer idDisplayLanguage;
	private Double accountBalance;
	private Double bankRecharge;
	private Double paypalRecharge;
	private Integer articlesInShoppingCart;
	private Integer unreadMessages;
	private Name name;
	private Address address;
	public Name getName() {
		return name;
	}
	public void setName(Name name) {
		this.name = name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Integer getIdUser() {
		return idUser;
	}
	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Boolean getIsCommercial() {
		return isCommercial;
	}
	public void setIsCommercial(Boolean isCommercial) {
		this.isCommercial = isCommercial;
	}
	public Integer getRiskGroup() {
		return riskGroup;
	}
	public void setRiskGroup(Integer riskGroup) {
		this.riskGroup = riskGroup;
	}
	public Integer getReputation() {
		return reputation;
	}
	public void setReputation(Integer reputation) {
		this.reputation = reputation;
	}
	public Integer getShipsFast() {
		return shipsFast;
	}
	public void setShipsFast(Integer shipsFast) {
		this.shipsFast = shipsFast;
	}
	public Integer getSellCount() {
		return sellCount;
	}
	public void setSellCount(Integer sellCount) {
		this.sellCount = sellCount;
	}
	public Boolean getOnVacation() {
		return onVacation;
	}
	public void setOnVacation(Boolean onVacation) {
		this.onVacation = onVacation;
	}
	public Integer getIdDisplayLanguage() {
		return idDisplayLanguage;
	}
	public void setIdDisplayLanguage(Integer idDisplayLanguage) {
		this.idDisplayLanguage = idDisplayLanguage;
	}
	public Double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(Double accountBalance) {
		this.accountBalance = accountBalance;
	}
	public Double getBankRecharge() {
		return bankRecharge;
	}
	public void setBankRecharge(Double bankRecharge) {
		this.bankRecharge = bankRecharge;
	}
	public Double getPaypalRecharge() {
		return paypalRecharge;
	}
	public void setPaypalRecharge(Double paypalRecharge) {
		this.paypalRecharge = paypalRecharge;
	}
	public Integer getArticlesInShoppingCart() {
		return articlesInShoppingCart;
	}
	public void setArticlesInShoppingCart(Integer articlesInShoppingCart) {
		this.articlesInShoppingCart = articlesInShoppingCart;
	}
	public Integer getUnreadMessages() {
		return unreadMessages;
	}
	public void setUnreadMessages(Integer unreadMessages) {
		this.unreadMessages = unreadMessages;
	}
}
