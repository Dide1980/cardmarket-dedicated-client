package ch.blanquet.mkm.beans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAnySetter;

public abstract class Element {
	protected static Logger logger = LoggerFactory.getLogger(Element.class);

	@JsonAnySetter
	public void handleUnknown(String key, Object value) {
		logger.warn(this.getClass().getName() + "::Unknown propertie " + key + " (" + value + ")");
	}
}
