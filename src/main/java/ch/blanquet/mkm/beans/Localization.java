package ch.blanquet.mkm.beans;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

@Entity
@Table(indexes = {@Index(name = "i1_name",  columnList="name")})
public class Localization extends Element implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 51037640898513773L;
	
	@Column(name = "name")
	private String name;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@JsonUnwrapped
	@ManyToOne(cascade=CascadeType.REFRESH)
	private MkmLocale locale;
	
	public MkmLocale getLocale() {
		return locale;
	}

	public void setLocale(MkmLocale locale) {
		this.locale = locale;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}
}
