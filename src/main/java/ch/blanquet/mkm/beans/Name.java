package ch.blanquet.mkm.beans;

import java.io.Serializable;

public class Name extends Element implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9210462386963028953L;
	private String firstName;
	private String lastName;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
