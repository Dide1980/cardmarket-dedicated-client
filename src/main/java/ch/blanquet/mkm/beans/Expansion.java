package ch.blanquet.mkm.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(indexes = {@Index(name = "i1_expansion_name",  columnList="expansion_name")})
public class Expansion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private Long expansionID;
	@Column(name = "expansion_name")
	private String expansionName;
	private Long expansionIcon;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Long getExpansionID() {
		return expansionID;
	}
	public void setExpansionID(Long expansionID) {
		this.expansionID = expansionID;
	}
	public String getExpansionName() {
		return expansionName;
	}
	public void setExpansionName(String expansionName) {
		this.expansionName = expansionName;
	}
	public Long getExpansionIcon() {
		return expansionIcon;
	}
	public void setExpansionIcon(Long expansionIcon) {
		this.expansionIcon = expansionIcon;
	}
}
