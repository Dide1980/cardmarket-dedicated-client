package ch.blanquet.mkm.routes;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

import org.apache.camel.Consume;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ch.blanquet.mkm.MkmClient;
import ch.blanquet.mkm.beans.Expansion;
import ch.blanquet.mkm.beans.Localization;
import ch.blanquet.mkm.beans.Metaproduct;
import ch.blanquet.mkm.beans.Product;
import ch.blanquet.mkm.beans.Response;
import ch.blanquet.mkm.repositories.ExpansionRepository;
import ch.blanquet.mkm.repositories.LocalizationRepository;
import ch.blanquet.mkm.repositories.MetaproductRepository;
import ch.blanquet.mkm.repositories.MkmLocaleRepository;
import ch.blanquet.mkm.repositories.ProductRepository;

@Component
public class Test extends RouteBuilder {

	@Autowired
	private MetaproductRepository metaproductRepository;

	@Autowired
	private LocalizationRepository localizationRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ExpansionRepository expansionRepository;

	@Autowired
	private MkmLocaleRepository mkmLocaleRepository;

	@Override
	public void configure() throws Exception {

		// restConfiguration().contextPath("/camel-rest-jpa").apiContextPath("/api-doc")
		// .apiProperty("api.title", "Camel REST API").apiProperty("api.version", "1.0")
		// .apiProperty("cors",
		// "true").apiContextRouteId("doc-api").bindingMode(RestBindingMode.json);

		// from("servlet://say/hello").to("direct:read.hello");
		// from("servlet://say/bye").to("direct:read.hello").to("direct:bye");

		restConfiguration().bindingMode(RestBindingMode.auto);

		rest("/say").get("/hello").to("direct:hello").get("/bye").consumes("application/json").to("direct:bye")
				.post("/bye").to("direct:bye");

		from("direct:hello").transform().constant("{ \"msg\" : \"Hello World\" }");
		from("direct:bye").transform().constant("{ \"msg\" : \"Bye World\" }");

		rest("/mkm").get("/account").to("direct:getAccount");
		rest("/mkm").get("/parseresponse").to("direct:parseAccount");
		rest("/mkm").get("/parseresponse2").to("direct:xmlToJson");
		rest("/mkm").get("/test?search={search}").route()
				.setBody(simple("https://api.cardmarket.com/ws/v2.0/productlist")).bean(MkmClient.class, "request")
				.unmarshal().jacksonxml().marshal().json();
		rest("/mkm").get("/productlist.raw").route().setBody(simple("https://api.cardmarket.com/ws/v2.0/productlist"))
				.bean(MkmClient.class, "request");
		rest("/mkm").get("/decodeproductlist").route().bean(TestBean.class, "test").unmarshal()
				.jacksonxml(Response.class).marshal().json();
		rest("/mkm").get("/decodeproductlist1").route()
				.setBody(simple("https://api.cardmarket.com/ws/v2.0/productlist")).bean(MkmClient.class, "request")
				.process(new Processor() {
					public void process(Exchange exchange) throws Exception {
						final String response = (String) exchange.getIn().getBody();

						final File file = new File("E:/workspaces/scala/sandbox/response.raw");
						final FileOutputStream fos = new FileOutputStream(file);

						fos.write(response.getBytes());

						fos.close();
					}
				}).unmarshal().jacksonxml(Response.class).marshal().json();

		rest("/mkm").get("/productlist.persists").route()
				.process(new ReadLocalFileProcessor("E:/workspaces/scala/sandbox/productlist.raw")).unmarshal()
				.jacksonxml(Response.class).process(new Processor() {

					@SuppressWarnings("unlikely-arg-type")
					public void process(Exchange exchange) throws Exception {
						final Response response = exchange.getIn().getBody(Response.class);

						for (final Product p : response.getProductsfile()) {
							System.out.println("ID: "+p.getIdProduct());
							
							if (!Optional.empty().equals(productRepository.findById(p.getIdProduct())))
								continue;
							
							for (final Localization l : p.getLocalizations()) {

								l.setLocale(mkmLocaleRepository.save(l.getLocale()));

								final Localization inBase = localizationRepository
										.findLocalization(l.getLocale().getIdLanguage(), l.getName());

								if (inBase == null)
									l.setId(localizationRepository.save(l).getId());
								else
									l.setId(inBase.getId());
							}

							if (p.getExpansion() != null) {
								final Expansion expansionEnBase = expansionRepository
										.findByExpansionID(p.getExpansion().getExpansionID());

								if (expansionEnBase == null) {
									p.getExpansion().setId(expansionRepository.save(p.getExpansion()).getId());
								} else {
									p.setExpansion(expansionEnBase);
								}

							}
							productRepository.save(p);
						}
					}
				}).marshal().json();

		rest("/mkm").get("/metaproducts").route()
				.setBody(simple("https://api.cardmarket.com/ws/v2.0/metaproducts/9074"))
				.bean(MkmClient.class, "request")
				.process(new DumpToFileProcessor("E:/workspaces/scala/sandbox/metaproducts.raw"));
		rest("/mkm").get("/metaproducts.local").route()
				.process(new ReadLocalFileProcessor("E:/workspaces/scala/sandbox/metaproducts.raw"));
		rest("/mkm").get("/metaproducts.deserialize").route()
				.process(new ReadLocalFileProcessor("E:/workspaces/scala/sandbox/metaproducts.raw")).unmarshal()
				.jacksonxml(Response.class).marshal().json();
		rest("/mkm").get("/metaproducts.persists").route()
				.process(new ReadLocalFileProcessor("E:/workspaces/scala/sandbox/metaproducts.raw")).unmarshal()
				.jacksonxml(Response.class).process(new Processor() {

					public void process(Exchange exchange) throws Exception {
						final Response response = exchange.getIn().getBody(Response.class);

						final Metaproduct metaproduct = response.getMetaproduct();

						for (final Localization l : metaproduct.getLocalizations()) {
							final Localization inBase = localizationRepository
									.findLocalization(l.getLocale().getIdLanguage(), l.getName());

							if (inBase == null)
								l.setId(localizationRepository.save(l).getId());
							else
								l.setId(inBase.getId());
						}
						metaproductRepository.save(metaproduct);

						for (final Product p : response.getProducts()) {
							for (final Localization l : p.getLocalizations()) {
								final Localization inBase = localizationRepository
										.findLocalization(l.getLocale().getIdLanguage(), l.getName());

								if (inBase == null)
									l.setId(localizationRepository.save(l).getId());
								else
									l.setId(inBase.getId());
							}

							final Expansion expansionEnBase = expansionRepository
									.findByExpansionName(p.getExpansion().getExpansionName());

							if (expansionEnBase == null) {
								p.getExpansion().setId(expansionRepository.save(p.getExpansion()).getId());
							} else {
								p.setExpansion(expansionEnBase);
							}

							productRepository.save(p);
						}
					}
				}).marshal().json();
	}

	private static class ReadLocalFileProcessor implements Processor {

		private final String filePath;

		public ReadLocalFileProcessor(String filePath) {
			super();
			this.filePath = filePath;
		}

		public void process(Exchange exchange) throws Exception {

			final File file = new File(filePath);
			final FileInputStream fos = new FileInputStream(file);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			IOUtils.copy(fos, baos);

			fos.close();

			exchange.getIn().setBody(new String(baos.toByteArray()));
		}
	}

	private static class DumpToFileProcessor implements Processor {

		private final String filePath;

		public DumpToFileProcessor(String filePath) {
			super();
			this.filePath = filePath;
		}

		public void process(Exchange exchange) throws Exception {
			final String response = (String) exchange.getIn().getBody();

			final File file = new File(filePath);
			final FileOutputStream fos = new FileOutputStream(file);

			fos.write(response.getBytes());

			fos.close();
		}
	}

	@Produce(uri = "direct:hello")
	private ProducerTemplate next;

	@Consume(uri = "direct:read.hello")
	public void test(final Object obj) {
		next.sendBody(obj);
	}

	public static class TestBean {
		public String test(Exchange exchange) throws IOException {

			final File f = new File("E:/workspaces/scala/sandbox/response.raw");
			final BufferedReader br = new BufferedReader(new FileReader(f));
			final String line = br.readLine();
			br.close();
			return line;
		}
	}
}
