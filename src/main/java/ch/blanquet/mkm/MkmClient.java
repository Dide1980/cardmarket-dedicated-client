package ch.blanquet.mkm;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.GregorianCalendar;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.springframework.stereotype.Service;

@Service
public class MkmClient {

	private final Credentials credentials;

	private Throwable _lastError;
	private int _lastCode;
	private String _lastContent;
	private boolean _debug;

	/**
	 * Constructor. Fill parameters according to given MKM profile app parameters.
	 * 
	 * @param appToken
	 * @param appSecret
	 * @param accessToken
	 * @param accessSecret
	 */
	public MkmClient(final Credentials credentials) {
		this.credentials = credentials;

		_lastError = null;
		_debug = false;

		System.setProperty("https.proxyHost", "bwf.int.ofac.ch");
		System.setProperty("https.proxyPort", "3128");
	}

	/**
	 * Activates the console debug messages
	 * 
	 * @param flag
	 *            true if you want to enable console messages; false to disable any
	 *            notification.
	 */
	public void setDebug(boolean flag) {
		_debug = flag;
	}

	/**
	 * Encoding function. To avoid deprecated version, the encoding used is UTF-8.
	 * 
	 * @param str
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String rawurlencode(String str) throws UnsupportedEncodingException {
		return URLEncoder.encode(str, "UTF-8");
	}

	private void _debug(String msg) {
		if (_debug) {
			System.out.print(GregorianCalendar.getInstance().getTime());
			System.out.print(" > ");
			System.out.println(msg);
		}
	}

	/**
	 * Get last Error exception.
	 * 
	 * @return null if no errors; instead the raised exception.
	 */
	public Throwable lastError() {
		return _lastError;
	}

	/**
	 * Perform the request to given url with OAuth 1.0a API.
	 * 
	 * @param requestURL
	 *            url to be requested. Ex.
	 *            https://api.cardmarket.com/ws/v2.0/products/island/1/1/false
	 * @return true if request was successfully executed. You can retrieve the
	 *         content with responseContent();
	 */
	public String request(String requestURL) throws Exception {
		_lastError = null;
		_lastCode = 0;
		_lastContent = "";
		try {

			_debug("Requesting " + requestURL);

			String realm = requestURL;
			String oauth_version = "1.0";
			String oauth_token = credentials.getAccessToken();
			String oauth_signature_method = "HMAC-SHA1";
			String oauth_timestamp = "" + (System.currentTimeMillis() / 1000L);
			// String oauth_timestamp = "1542611265";
			String oauth_nonce = (Math.random() + "").substring(2);
			// String oauth_nonce = "5526977850771642";

			String encodedRequestURL = rawurlencode(requestURL);

			String baseString = "GET&" + encodedRequestURL + "&";

			String paramString = "oauth_consumer_key=" + rawurlencode(credentials.getAppToken()) + "&" + "oauth_nonce="
					+ rawurlencode(oauth_nonce) + "&" + "oauth_signature_method=" + rawurlencode(oauth_signature_method)
					+ "&" + "oauth_timestamp=" + rawurlencode(oauth_timestamp) + "&" + "oauth_token="
					+ rawurlencode(oauth_token) + "&" + "oauth_version=" + rawurlencode(oauth_version);

			baseString = baseString + rawurlencode(paramString);

			String signingKey = rawurlencode(credentials.getAppSecret()) + "&"
					+ rawurlencode(credentials.getAccessTokenSecret());

			HmacUtils hmacUtils = new HmacUtils(HmacAlgorithms.HMAC_SHA_1, signingKey);

			byte[] digest = hmacUtils.hmac(baseString.getBytes());
			String oauth_signature = java.util.Base64.getEncoder().encodeToString(digest); // Base64.encode(digest) ;

			String authorizationProperty = "OAuth realm=\"" + realm + "\", " + "oauth_version=\"" + oauth_version
					+ "\", " + "oauth_timestamp=\"" + oauth_timestamp + "\", " + "oauth_nonce=\"" + oauth_nonce + "\", "
					+ "oauth_consumer_key=\"" + credentials.getAppToken() + "\", " + "oauth_token=\"" + oauth_token
					+ "\", " + "oauth_signature_method=\"" + oauth_signature_method + "\", " + "oauth_signature=\""
					+ oauth_signature + "\"";

			HttpURLConnection connection = (HttpURLConnection) new URL(requestURL).openConnection();
			connection.addRequestProperty("Authorization", authorizationProperty);
			connection.connect();

			// from here standard actions...
			// read response code... read input stream.... close connection...

			_lastCode = connection.getResponseCode();

			_debug("Response Code is " + _lastCode);

			if (200 == _lastCode || 404 == _lastCode) {
				BufferedReader rd = new BufferedReader(new InputStreamReader(
						_lastCode == 200 ? connection.getInputStream() : connection.getErrorStream()));
				StringBuffer sb = new StringBuffer();
				String line;
				while ((line = rd.readLine()) != null) {
					sb.append(line);
				}
				rd.close();
				_lastContent = sb.toString();
				return _lastContent;
			} else if (401 == _lastCode) {
				throw new Exception(connection.getResponseMessage());
			}

			throw new Exception("Error "+_lastCode+" while invoking "+requestURL);
		} catch (Exception e) {
			_debug("(!) Error while requesting " + requestURL);
			_lastError = e;
			throw e;
		}

	}

	/**
	 * Get response code from last request.
	 * 
	 * @return
	 */
	public int responseCode() {
		return _lastCode;
	}

	/**
	 * Get response content from last request.
	 * 
	 * @return
	 */
	public String responseContent() {
		return _lastContent;
	}

	public static void main(String[] args) throws Exception {

		if (args.length == 0) {
			throw new IllegalArgumentException("Need a credentials properties file in parameter");
		}

		File credentialsFile = new File(args[0]);

		MkmClient app = new MkmClient(new Credentials(credentialsFile));
		app.setDebug(true);

		// https://api.cardmarket.com/ws/v2.0/priceguide

		final String result = app.request("https://api.cardmarket.com/ws/v1.1/account");
		if (result != null) {
			System.out.println(result);
		}

		/*
		 * if (app.request("https://api.cardmarket.com/ws/v2.0/account")) {
		 * System.out.println(app.responseContent()); }
		 */

		// test with active console debug
		/*
		 * app.setDebug(true); if
		 * (app.request("https://api.cardmarket.com/ws/v2.0/products/island/1/1/false"))
		 * { // .. process(app.responseContent()); }
		 * 
		 * if (app.request(
		 * "https://api.cardmarket.com/ws/v2.0/products/serra_angel/1/1/false")) { // ..
		 * process(app.responseContent()); }
		 */

		// etc....
	}
}
