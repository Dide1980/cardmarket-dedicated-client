package ch.blanquet.mkm;

import java.io.IOException;
import java.util.Locale;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

@Component
public class LocaleDeserializer extends JsonDeserializer<Locale> {

	private final static Locale[] supportedLocales = new Locale[] { Locale.ENGLISH, Locale.FRENCH, Locale.GERMAN,
			new Locale("es", "ES"), Locale.ITALIAN };

	@Override
	public Locale deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		return deserialize(p.getValueAsString());
	}

	public static Locale deserialize(final String value) throws IOException {
		for (final Locale locale : supportedLocales) {
			if (locale.getDisplayLanguage(Locale.ENGLISH).equalsIgnoreCase(value))
				return locale;
		}

		throw new IOException("Unknow language : " + value);
	}
}
