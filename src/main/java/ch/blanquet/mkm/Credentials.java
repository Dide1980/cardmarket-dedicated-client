package ch.blanquet.mkm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Credentials {
	private String appToken;
	private String appSecret;
	private String accessToken;
	private String accessTokenSecret;

	private boolean initialized = false;

	@Value("${ch.blanquet.mkmclient.credentials}")
	private String credentialsFile;

	public Credentials() {

	}

	public Credentials(final File credentialsFile) throws FileNotFoundException, IOException {
		init(credentialsFile);
	}

	private void init() throws FileNotFoundException, IOException {
		if (!initialized)
			init(new File(credentialsFile));
	}

	private void init(final File credentialsFile) throws FileNotFoundException, IOException {
		if (!credentialsFile.exists()) {
			throw new IllegalArgumentException(
					"The credentials properties file " + credentialsFile + " does not exists");
		}

		Properties credentials = new Properties();
		credentials.load(new FileInputStream(credentialsFile));

		setAccessToken(credentials.getProperty("access_token"));
		setAccessTokenSecret(credentials.getProperty("access_token_secret"));
		setAppSecret(credentials.getProperty("app_secret"));
		setAppToken(credentials.getProperty("app_token"));

		this.initialized = true;
	}

	public String getAppToken() throws FileNotFoundException, IOException {
		init();
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	public String getAppSecret() throws FileNotFoundException, IOException {
		init();
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getAccessToken() throws FileNotFoundException, IOException {
		init();
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAccessTokenSecret() throws FileNotFoundException, IOException {
		init();
		return accessTokenSecret;
	}

	public void setAccessTokenSecret(String accessTokenSecret) {
		this.accessTokenSecret = accessTokenSecret;
	}
}
